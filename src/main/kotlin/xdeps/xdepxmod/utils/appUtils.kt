package xdeps.xdepxmod.utils

import java.nio.file.Paths
import kotlin.io.path.exists

/**
 * Retorna true caso o módulo esteja sendo inicializado (init)
 */
fun isInitExecution(vararg args: String?) = args.isNotEmpty() && args[0] == "init"

/**
 * Retorna true caso o arquivo descritor xtool.yaml exista no diretório de execução (workspace.path)
 */
fun xtoolDescritorExists() = Paths.get(System.getProperty("user.dir")).resolve("xtool.yaml").exists()
