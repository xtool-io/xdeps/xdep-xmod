package xdeps.xdepxmod

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import xdeps.xdepxmod.utils.isInitExecution
import xdeps.xdepxmod.utils.xtoolDescritorExists
import kotlin.system.exitProcess

@Configuration
@ComponentScan
class XdepXmod

val log: Logger = LoggerFactory.getLogger(XdepXmod::class.java)

fun runXModule(moduleClass: Class<*>, vararg args: String) {
    if (!isInitExecution(*args)) {
        if (!xtoolDescritorExists()) {
            log.error("""Não foi possível localizar o arquivo xtool.yaml em ${System.getProperty("user.dir")}""")
            exitProcess(1)
        }
    }
    SpringApplicationBuilder(moduleClass)
        .logStartupInfo(false)
        .headless(false)
        .run(*args)
}

