package xdeps.xdepxmod.command

import picocli.CommandLine.Unmatched

abstract class XInitializer : Runnable {
    @Unmatched
    lateinit var unmatched: List<String>
}
