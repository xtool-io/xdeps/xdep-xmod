package xdeps.xdepxmod.config

import org.fusesource.jansi.Ansi
import org.fusesource.jansi.AnsiConsole
import org.jline.console.impl.SystemRegistryImpl
import org.jline.reader.*
import org.jline.reader.impl.DefaultParser
import org.jline.terminal.TerminalBuilder
import org.jline.widget.AutosuggestionWidgets
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.ExitCodeGenerator
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import picocli.CommandLine
import picocli.shell.jline3.PicocliCommands
import xdeps.xdepxmod.command.RootCommand
import xdeps.xdepxmod.command.XCommand
import xdeps.xdepxmod.command.XInitializer
import xdeps.xdepxmod.utils.isInitExecution
import java.nio.file.Paths
import java.util.function.Supplier


@Configuration
class CommandLineRunnerConfig(
    private var factory: CommandLine.IFactory,
    private var rootCommand: RootCommand,
    private val commands: List<XCommand>,
    private var initializer: XInitializer,
    private val env: Environment,
) : CommandLineRunner, ExitCodeGenerator {


    private var exitCode = 0
    override fun run(vararg args: String?) {
        AnsiConsole.systemInstall();
        try {
            if (isInitExecution(*args)) {
                CommandLine(initializer, factory).execute(*args.drop(1).toTypedArray())
                return
            }
            println("""Módulo: ${Ansi.ansi().fgBrightCyan().bold().a(env.getProperty("module.name")).a("@").a(env.getProperty("module.version")).reset()}""")
            println("")
            val workDir = Paths.get(System.getProperty("user.dir"))
            val pathSupplier = Supplier { workDir }
            // let picocli parse command line args and run the business logic
            val cmd = CommandLine(rootCommand, factory)
            commands.forEach { cmd.addSubcommand(it) }
            val picocliCommands = PicocliCommands(cmd)
            val parser = DefaultParser()
            val terminal = TerminalBuilder.builder().build()
            val systemRegistry = SystemRegistryImpl(parser, terminal, pathSupplier, null)
            systemRegistry.setCommandRegistries(picocliCommands)
            systemRegistry.register("help", picocliCommands)
            val reader = LineReaderBuilder.builder()
                .terminal(terminal)
                .completer(systemRegistry.completer())
                .parser(parser)
                .variable(LineReader.LIST_MAX, 50) // max tab completion candidates
                .build()
            val widgets = AutosuggestionWidgets(reader)
            widgets.enable()
            val prompt = Ansi.ansi().bold().fgBrightGreen().a("xctl@${workDir.toFile().name}# ").reset().toString()
            val rightPrompt = null
            var line: String?
            while (true) {
                try {
                    systemRegistry.cleanUp()
                    line = reader.readLine(prompt, rightPrompt, null as MaskingCallback?, null)
                    systemRegistry.execute(line)
                } catch (e: UserInterruptException) {
                    // Ignore
                } catch (e: EndOfFileException) {
                    return
                } catch (e: Exception) {
                    systemRegistry.trace(e)
                }
            }
        } catch (t: Throwable) {
            t.printStackTrace();
        } finally {
            AnsiConsole.systemUninstall();
        }
    }

    override fun getExitCode(): Int {
        return exitCode
    }

}
